'use strict'
const 
    router = require('express').Router(),
    webdriver = require('../controllers/webdriver'),
    search = require('../controllers/search'),
    save = require('../controllers/save')


router
    .get('/',(req,res) => {res.send('Ready')})
    /**
     * Selenium Webdriver 
     * id = Numero de placa
     */
    .get('/search/:id/',
        webdriver.getDriver,
        search.init,
        save.saveData
    )


module.exports = router