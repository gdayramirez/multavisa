'use strict';

const 
    express = require('express'),
    bodyParser = require('body-parser'),
    app = express(),
    router = require('./routes/router'),
    port = 3500

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use('/', express.static('public'))
app.use('/',router)

app.listen(port, function() {
	console.log('webserver listening in port --->' + port)
})
