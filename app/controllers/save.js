'use strict'
const 
    pg = require('pg'),
    connectionString = 'postgres://gustavo@localhost:5432/gustavo',
    async = require('async')
exports.saveData = (req,res,next)=>{
    console.log('saveData')
    let results = []
    pg.connect(connectionString, (err, client, done) => {
        if(!err){
            async.eachSeries(req.data,(e,callback)=>{
                if (e['1'] == 'Boleta'){
                    console.log('Aqui no hare nada')
                    callback()
                }
                else{
                    client.query('INSERT INTO multavisa(boleta,placa,fecha,infraccion,descripcion,monto) values($1,$2,$3,$4,$5,$6)',[e['1'], e['2'], e['3'], e['4'], e['5'], e['6']]);
                    results.push({
                        save : 'ok',
                        data : e
                    })
                    callback()
                }
            },()=>{
                console.log('termine')
                res.send({message : 'ok',total: results.length,results : results})
            })
        }
        else{
            console.log('!Conectado--->'+err)
        }
    })
}