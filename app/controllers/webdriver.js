'use strict'
/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.getDriver = (req,res,next)=>{
    const
    webdriver = require('selenium-webdriver'),
    chromeCapabilities = webdriver.Capabilities.chrome(),
    chromeOptions = {'args': ['--ingognito','--dns-prefetch-disable']}
    /**
     * chromeCapabilities -> set chrome options
     */
    chromeCapabilities.set('chromeOptions', chromeOptions)
    /**
     * Initialize 
     */
    req.webdriver = webdriver
    req.By        = webdriver.By,
    req.until     = webdriver.until
    req.driver    = new webdriver.Builder().forBrowser('chrome').usingServer('http://localhost:4444/wd/hub').withCapabilities(chromeCapabilities).build()
    req.RETURN    = webdriver.Key.ENTER
    next()
} 

  