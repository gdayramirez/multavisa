'use strict'
const parseHtml = require('html-table-to-json')
/**
 * Buscar numero de placa
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.init = (req,res,next)=>{
    let id = req.params.id
    console.log('INIT')
    req.driver.get('http://servicios.monterrey.gob.mx/consultas/transito/Reportes/Infracciones.aspx')
    .then(()=>{
        return req.driver.wait(req.until.elementLocated(req.By.id('ctl00_ContentPlaceHolder1_txtNumPlaca')),10000)
    })
    .then(()=>{
        return req.driver.findElement(req.By.id('ctl00_ContentPlaceHolder1_txtNumPlaca')).sendKeys(id)
    })
    .then(()=>{
        return req.driver.findElement(req.By.id('ctl00_ContentPlaceHolder1_btnBuscar')).click()
    })
    .then(()=>{
        return req.driver.findElement(req.By.xpath('//*[@id="ctl00_ContentPlaceHolder1_pnlReporte"]/table/tbody/tr[1]/td'))
    })
    .then((html)=>{
        return html.getAttribute("innerHTML") 
    })
    .then((innerhtml)=>{
        req.driver.close()
        let json = new parseHtml(innerhtml);
        json = json._results.shift()
        req.data = json
        next()
    })
    .catch(error =>{
        req.driver.close()
        console.log('Error : '+error)
        return res.send({
            message : 'Error',
            error : 'error :'+error,
            status : 400
        }).status(400)
    })
}
